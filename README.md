# voltron-startup

---

This is a script that opens 8 tmux panes and puts startup commands in em.

***Here's how to use it:***
- Supply a file as the first and only argument
	- The script reads the commands in it line-by-line and throws each one in its own tmux pane
- `voltronrc` gets sourced in ***every single pane*** before those get put in there tho
