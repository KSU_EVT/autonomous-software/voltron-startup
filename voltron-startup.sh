#!/bin/bash

# Read commands from file specified in first argument
readarray -t a < $1

# Create detached session
tmux new-session -d

# Create Panes
tmux split-window -v
tmux select-pane -t 0
tmux split-window -h -p 75
tmux split-window -h -p 75
tmux split-window -h -p 75
tmux select-pane -t 4
tmux split-window -h -p 75
tmux split-window -h -p 75
tmux split-window -h -p 75

# Run voltronrc in all panes
tmux setw synchronize-panes on
tmux send-keys "source voltronrc" Enter
tmux setw synchronize-panes off

# Send commands to panes
tmux select-pane -t 0
tmux send-keys "${a[0]}"
tmux select-pane -t 1
tmux send-keys "${a[1]}"
tmux select-pane -t 2
tmux send-keys "${a[2]}"
tmux select-pane -t 3
tmux send-keys "${a[3]}"
tmux select-pane -t 4
tmux send-keys "${a[4]}"
tmux select-pane -t 5
tmux send-keys "${a[5]}"
tmux select-pane -t 6
tmux send-keys "${a[6]}"
tmux select-pane -t 7
tmux send-keys "${a[7]}"


tmux attach-session
